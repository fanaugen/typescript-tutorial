const express = require('express');
const crypto = require('crypto');
const fetch = require('node-fetch');
const app = express();
const port = 4000;


let db = {};

function getSHA1(input){
  return crypto.createHash('sha1').update(input).digest('hex');
}

function minute() {
  let d = new Date();
  d.setTime(d.getTime() + 60000);
  return d.toISOString();
}

function two() {
  let d = new Date();
  d.setTime(d.getTime() + 120000);
  return d.toISOString();
}

function webhook(response) {
  if (response.customer_id.endsWith("e")) {
    setTimeout(() => {
      response.subscription.expires_at = new Date();
      fetch("http://payment-service:3000/webhook", {
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify(response)
      });
    }, 500);
  }
}

app.use(express.json());

app.get('/subscriptions/:subscriptionId', (req, res) => {
  const record = db[req.params.subscriptionId];
  if (record === undefined) {
    res.sendStatus(404);
  } else {
    res.set('Content-Type', 'application/json');
    res.send(JSON.stringify(record));
  }
});

app.post('/purchases', (req, res) => {
  const purchase = req.body;
  if (purchase.token === "valid") {
    if (purchase.sku === "blinkist-60-seconds" || purchase.sku === "blinkist-2-minutes") {
      const subscription_id = getSHA1(purchase.customer_id + ":" + purchase.sku);
      const expires_at = purchase.sku === "blinkist-60-seconds" ? minute() : two();
      const subscription = {
        subscription_id: subscription_id,
        expires_at: expires_at
      };

      const response = {
        sku: purchase.sku,
        customer_id: purchase.customer_id,
        subscription: subscription
      };

      webhook(response);

      db[subscription_id] = subscription;


      res.set('Content-Type', 'application/json');
      res.send(JSON.stringify(response));
    } else {
      res.sendStatus(422);
    }
  } else {
    res.sendStatus(403);
  }
});

app.listen(port, () => console.log(`Vendor service running on port ${port}!`));
