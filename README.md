# Introduction

Welcome to the TypeScript Experience, a learning session designed to give you a deep dive into the TypeScript language and its ecosystem.

The core of the session revolves around programming one of the services in the system. More specifically we will be working on a payment service that processes payment requests and orchestrates other services.

Our system consists of the following services:

* `payment-service` - **the one we will be working on**
* `access-service` - already provided service that grants users access
* `vendor-service` - already provided service that simulates a payment vendor and processes payments
* `mysql` - a database to be used by `payment-service`
* `redis` - a key-value store

# Starting the system

To start the service use the command in a terminal:

```bash
docker-compose up --build payment-service
```

This will bring up the `payment-service` in the foreground and run its dependencies (other services) in the background.

## Changing code

The service `payment-service` is setup in such a way that if any `*.ts` file under `/payment-service/app` is edited **on your machine** the service will be automatically restarted, so that the code changes are reflected.

The restart usually takes about a second or two.

## Changing `payment-service`

If you want to add new packages, change the configuration files or somehow you do not see the result of your modifications then you may need to rebuild the container.

This is as easy as stopping the previously invoked `docker-compose up` and running it again.

Should you find yourself repeating this command over and over **ask for help**! This should not be the case and something is wrong.

# General design

The point of the `purchase-service` is to enable purchases and orchestrate other services.

The main entry point of `purchase-service` is the purchase operation which takes a `user_id`, some sort of purchase `token` and `sku`.

The exact algorithm is as follows:

1. Input: `user_id, token, sku`
2. Call `vendor-service` to acquire `expires_at` and `subscription_id`
3. Call `access-service` to grant the user access until `expires_at`
4. Store `subscription_id, user_id, sku, expires_at` in the database
5. Output: something nice

# Interfaces

The following section briefly explains the interfaces that the provided services expose and expect.

The servicess exchange data using JSON encoded inside the request body and request response, respectively.

Their API follow the usual REST semantics.

## `access-service`

This service is used to grant users a premium/paid access to our system.

### `POST /accesses`

Input:

```json
{
	"user_id": "some-string-user-id",
	/* date as string in ISO format
	"expires_at": "2018-12-05T10:33:10.869Z"
}
```

Output:

```json
{
	"user_id": "some-string-user-id",
	/* date as string in ISO format
	"expires_at": "2018-12-05T10:33:10.869Z"
}
```

Yes, it is essentially a ping service. But it will try to very naively validate the passed date.

## `vendor-service`

This service is used for all aspects of payment processing.

### `POST /purchases`

Do a purchase with a valid token. It will return the expiration date of the subscription as the response.

Input:

```json
{
	/* only "valid" is a valid token */
	"token": "some-token-value",
	/* "blinkist-60-seconds" | "blinkist-60-seconds" */
	"sku": "some-sku-value",
	"customer_id": "some-user-id"
}
```

Output:

```json
{
	/* the same sku as input */
	"sku": "valid-sku",
	/* the same customer_id as input */
	"customer_id": "some-user-id",
	"subscription": {
		/* date as string in ISO format
		"expires_at": "2018-12-05T10:33:10.869Z"
		/* a hexadecimal ID of 40 characters */
		"subscription_id": "c22b5f9178342609428d6f51b2c5af4c0bde6a42"
	}
}
```


# Diving in

The following set of exercises will be performed together in an interactive format.

### `AccessService` refactoring

`AccessService` class contains a utility function `parseDate`. Since we are dealing with dates a lot in this project we probably want to extract `parseDate` into a separate module under `/payment-service/app/utils`.

Do not forget to take `InvalidDateError` with you!

### `PurchasesController` refactoring

The private module functions `validateSku` and `validateStringPresence` are currently inside the `PurchasesController` module. It is reasonable to assume that we will want to reuse them in other places. Let us extract these function into a separate module under `/payment-service/app/utils`.

### Database persistance

The function `storePurchase` inside `PurchaseService` doesn't do anything at the moment. Let us complete it so that it stores a purchase to the database.

### Database reading

The controller `PurchasesController` exposes a dummy `GET` operation on `/`. Our goal is to make it useful and return all `Purchase` objects from the database in JSON format.

### Completing `VendorService`

The class `VendorService` exposes two methods: `purchase` and `lookup` without implementation. Have a look first at `create` inside `AccessService` and try implementing the missing methods!

# Group work

Now it is time to work in groups. Together you will use what you have learned and the provided material to try to complete the purchase service.

### Completing `PurchaseService`

The `PurchaseService` does not have any useful code inside `purchase` method. Use the information found in *General design* section to implement it.

### Adding error handling

At the moment the `PurchasesController` does not do much error handling. If anything goes wrong it essentially returns 500 which is not very client friendly. Think about the things that can go wrong and implement appropriate error handling!

# Bonus 1

If you purchase with a user whose ID ends with an `e` you may find yourself receiving a web hook from `vendor-service`! Look into `WebhookController` and try to handle the new development!

# Bonus 2

The subscriptions in the database expire and we are not renewing them. Now the going gets though. You will need to implement scheduled background processing that calls `vendor-service` after a subscription has expired to potentially renew it.

The following are some background processing frameworks for node.js:

* [https://github.com/OptimalBits/bull]()
* [https://github.com/Automattic/kue]()
* [https://github.com/taskrabbit/node-resque]()
* [https://github.com/bee-queue/bee-queue]()
