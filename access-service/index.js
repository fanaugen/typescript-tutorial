const express = require('express');
const app = express();
const port = 5000;

app.use(express.json())

app.post('/accesses', (req, res) => {
  const input = req.body;
  console.log("Access requested: " + JSON.stringify(input));

  const output = {
    user_id: input.user_id,
    expires_at: (new Date(input.expires_at)).toISOString()
  }

  res.set('Content-Type', 'application/json');
  res.send(JSON.stringify(output));
})

app.listen(port, () => console.log(`Access service running on port ${port}!`));
