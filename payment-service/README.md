# Payment Service

This service is handling payments and orchestrating access to the system.

## Setup

You do not need to setup anything besides the general setup of the system with `docker-compose`.

If you have installed `node.js` and want to use the tools on your machine then run:

```
npm install
```

Several useful commands are provided:

* `npm run tsc` will compile all `*.ts` files into `*.js` and store them in `build` directory. It is unneccessary but can be useful and interesting ("does it compile?").
* `npm run watch` will start the web server and watch for changes in `*.ts` files. Whenever such change occurs it will restart the server. **NOTE**: the database configuration inside `ormconfig.json` may need to be adjusted for this to work!
* `npm run ts-node` to spawn interactive TypeScript interpreter

## Structure

Project description and its dependencies can be found in `package.json`.

All the application code lives inside `app` directory.

`server.ts` is the main entry point of the application. It stats the webserver and serves the requests.

`app/controllers` contains all the controllers in our services. Because of express architecture they are actually routes but we call them controllers nevertheless.

`app/entities` contains all entities that we will use to interact with the database through TypeORM.

`app/services` contains all the services.

`app/utils` contains miscelanious functions that are useful in many places but haven't found a better home.

## Testing

Next time, I promise!