import { postJson, parseDate } from '../utils';

export interface SubscriptionAccessRequest {
    user_id: string,
    expires_at: Date
}

interface RawSubscriptionAccessResponse {
    user_id?: string,
    expires_at?: string
}

export interface SubscriptionAccessResponse extends SubscriptionAccessRequest {}

export class AccessService {
    constructor(private serviceUrl: string = "http://access-service:5000") {}

    async create(access: SubscriptionAccessRequest): Promise<SubscriptionAccessResponse> {
        const { user_id, expires_at } = await postJson<RawSubscriptionAccessResponse>(this.createUrl(), access);

        // parse & repackage
        return {
            user_id: String(user_id),
            expires_at: parseDate(expires_at)
        };
    }

    createUrl(): string {
        return this.serviceUrl + "/accesses"
    }
}