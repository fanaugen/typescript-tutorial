import { postJson, getJson, validateSku, validateStringPresence, parseDate } from "../utils";

export type Sku = "blinkist-60-seconds" | "blinkist-2-minutes";

export interface PurchaseRequest {
    sku: Sku,
    customer_id: string,
    token: string
}

interface RawPurchaseResponse {
    sku?: string,
    customer_id?: string,
    subscription: RawSubscriptionLookupResponse
}

export interface SubscriptionLookupRequest {
    subscription_id: string
}

interface RawSubscriptionLookupResponse {
    subscription_id?: string,
    expires_at?: string
}

export interface PurchaseResponse {
    sku: Sku;
    customer_id: string;
    subscription: SubscriptionLookupResponse;
}

export interface SubscriptionLookupResponse {
    subscription_id: string,
    expires_at: Date
}

export interface SubscriptionWebhook extends SubscriptionLookupResponse {}

export class VendorService {
    constructor(private serviceUrl: string = "http://vendor-service:4000") {}

    async purchase(purchase: PurchaseRequest): Promise<PurchaseResponse> {
        const {
            sku, customer_id, subscription
        } = await postJson<RawPurchaseResponse>(this.purchaseUrl(), purchase);

        if (typeof subscription !== "object" || subscription === undefined) {
            throw new Error('Expected subscription data in the response but got garbage');
        }
        return {
            sku: validateSku(sku),
            customer_id: validateStringPresence(customer_id),
            subscription: {
                subscription_id: validateStringPresence(subscription.subscription_id),
                expires_at: parseDate(subscription.expires_at)
            }
        };
    }

    async lookup(subscription: SubscriptionLookupRequest): Promise<SubscriptionLookupResponse> {
        const {
            subscription_id, expires_at
        } = await getJson<RawSubscriptionLookupResponse>(this.lookupUrl(subscription.subscription_id));

        return {
            subscription_id: validateStringPresence(subscription_id),
            expires_at: parseDate(expires_at)
        }
    }

    purchaseUrl(): string {
        return this.serviceUrl + "/purchases";
    }

    lookupUrl(id: string): string {
        return this.serviceUrl + "/subscriptions/" + id;
    }
}