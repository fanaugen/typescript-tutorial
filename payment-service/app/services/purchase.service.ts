import { AccessService } from "./access.service";
import { VendorService, PurchaseRequest } from "./vendor.service";
import { Purchase } from "../entities";
import { getRepository, Repository } from "typeorm";

export class PurchaseService {
    private accessService: AccessService;
    private vendorService: VendorService;

    constructor(assesServiceUrl: string | undefined = undefined, vendorServiceUrl: string | undefined = undefined) {
        this.accessService = new AccessService(assesServiceUrl);
        this.vendorService = new VendorService(vendorServiceUrl);
    }

    async purchase(purchaseRequest: PurchaseRequest): Promise<Purchase> {
        // STEP 1: call vendor service
        const purchaseResponse = await this.vendorService.purchase(purchaseRequest);

        // STEP 2: call access service
        const accessResponse = await this.accessService.create({
            user_id: purchaseResponse.customer_id,
            expires_at: purchaseResponse.subscription.expires_at
        })

        // STEP 3: use storePurchase()
        // STEP 4: return a Purchase
        return this.storePurchase({
            user_id: accessResponse.user_id,
            sku: purchaseResponse.sku,
            subscription_id: purchaseResponse.subscription.subscription_id,
            expires_at: accessResponse.expires_at
        });
    }

    async storePurchase(purchase: Purchase): Promise<Purchase> {
        return getRepository(Purchase).save(purchase);
    }
}