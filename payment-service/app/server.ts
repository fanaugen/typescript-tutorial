// we need this for @annotations
import "reflect-metadata";

// Import everything from express
import express from 'express';

// We need this to establish DB connection
import {createConnection} from "typeorm";

// Import the controllers we use
import {
    PingController, WebhookController, PurchasesController
} from './controllers';

// Create a new express application
const app: express.Application = express();
// The port the express app will listen on
const port: number = +(process.env.PORT || 3000);

// Mount the PingController at the /ping route
app.use('/ping', PingController);

// Mount the PurchasesController at the /purchases route
app.use('/purchases', PurchasesController);

// Mount the WebhookController at the /webhook route
app.use('/webhook', WebhookController);

// Connect to the DB first
createConnection().then((connection) => {
    // Serve the application at the given port
    app.listen(port, () => {
        // Server started!
        console.log(`Listening at http://localhost:${port}/`);
    });
})

