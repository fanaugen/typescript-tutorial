import {Entity, Column, PrimaryColumn} from "typeorm";

@Entity()
export class Purchase {
    @Column()
    user_id: string;

    @Column()
    sku: string;

    @PrimaryColumn()
    subscription_id: string;

    @Column("datetime")
    expires_at: Date;

}