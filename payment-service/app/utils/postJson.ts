import fetch, { Response } from 'node-fetch';

export default async function postJson<T>(url: string, data: object): Promise<T> {
    const response: Response = await fetch(url, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    });

    // wait for the body to be parsed into JSON
    return await response.json() as T;
}