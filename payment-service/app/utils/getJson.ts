import fetch, { Response } from 'node-fetch';

export default async function getJson<T>(url: string): Promise<T> {
    const response: Response = await fetch(url, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    });

    // wait for the body to be parsed into JSON
    return await response.json() as T;
}