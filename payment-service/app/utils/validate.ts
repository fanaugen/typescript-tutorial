import { Sku } from '../services';

// ensure that we are receiving a valid sku
//
// PurchaseRequest['sku'] ensures that the return of the function
// will be the type that the property sku of PurchaseRequest expects
// validate that input is a valid sku
export function validateSku(input: any): Sku {
    if (input === 'blinkist-60-seconds' || input === 'blinkist-2-minutes') {
        return input;
    }
    throw new Error(`Invalid sku: ${input}`);
}

// Ensure that a string (!!) is present and nonempty
export function validateStringPresence(input: any): string {
    if (typeof input === "string" && input !== "") {
        return input;
    }
    throw new Error(`Expected nonempty string but received: ${input}`);
}
