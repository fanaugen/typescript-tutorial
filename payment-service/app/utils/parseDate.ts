export class InvalidDateError extends Error {}

export function parseDate(input: any): Date {
    if (typeof input === "string") {
        const date: Date = new Date(input);
        if (!isNaN(date.getTime())) {
            return date;
        }
    }
    throw new InvalidDateError(`Unable to parse input ${input}`)
}