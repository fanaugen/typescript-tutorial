import { Router, Request, Response } from 'express';
import bodyParser from 'body-parser';

// this will be used as our controller
const router: Router = Router();

// this middleware will parse JSON body
router.use(bodyParser.json());

// no matter the method this is how we handle it
router.all('/', (req: Request, res: Response) => {
    // dump the input to console
    console.log("Webhook received:");
    console.log(JSON.stringify(req.body));
    res.sendStatus(200);
});

// export our router as WebhookController
export const WebhookController: Router = router;
