// express components
import { Router, Request, Response } from 'express';
// JSON parsing middleware
import bodyParser from 'body-parser';

import { PurchaseRequest, PurchaseService, Sku } from '../services'
import { Purchase } from '../entities';
import { getRepository } from 'typeorm';
import { validateSku, validateStringPresence } from '../utils'; // index.ts is the default

function purchaseRequestFromRequest(req: Request): PurchaseRequest {
    const { sku, customer_id, token } = req.body;

    return {
        sku: validateSku(sku),
        customer_id: validateStringPresence(customer_id),
        token: validateStringPresence(token)
    };
}

// this will be used as our controller
const router: Router = Router();

// this middleware will parse JSON body
router.use(bodyParser.json());

router.post('/', async (req: Request, res: Response) => {
    try {
        const purchaseRequest = purchaseRequestFromRequest(req);

        const purchaseService = new PurchaseService();
    
        const purchase: Purchase = await purchaseService.purchase(purchaseRequest);

        res.set('Content-Type', 'application/json');
        res.send(JSON.stringify(purchase));
    } catch (e) {
        console.error(JSON.stringify(e));
        res.sendStatus(500);
    }
});

router.get('/', async (req: Request, res: Response) => {
    const purchases: Purchase[] = await getRepository(Purchase).find();
    res.set('Content-Type', 'application/json');
    res.send(JSON.stringify(purchases));
});

// export our router as PurchasesController
export const PurchasesController: Router = router;