import { Router, Request, Response } from 'express';

// this will be used as our controller
const router: Router = Router();

router.get('/', (_req: Request, res: Response) => {
    res.set('Content-Type', 'text/plain');
    res.send('pong')
});

// export our router as PingController
export const PingController: Router = router;